linux:
  system:
    enabled: true
    cluster: default
    name: {{ grains['fqdn'] }}
    domain: local
    environment: prd
    timezone: 'Etc/UTC'
    utc: true
    locale:
      en_US.UTF-8:
        default: true
  network:
    enabled: true
    hostname: {{ grains['fqdn'] }}
